/* tslint:disable */
/* eslint-disable */
/**
 * ChiRhos API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: v1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


import * as runtime from '../runtime';
import {
    ApiRequest,
    ApiRequestFromJSON,
    ApiRequestToJSON,
    IRelationshipTypeIStorableIDataResponse,
    IRelationshipTypeIStorableIDataResponseFromJSON,
    IRelationshipTypeIStorableIDataResponseToJSON,
    IRelationshipTypeIStorableIEnumerableIDataResponse,
    IRelationshipTypeIStorableIEnumerableIDataResponseFromJSON,
    IRelationshipTypeIStorableIEnumerableIDataResponseToJSON,
    RelationshipTypeConstruct,
    RelationshipTypeConstructFromJSON,
    RelationshipTypeConstructToJSON,
} from '../models';

export interface ApiRelationshipTypeDeleteRequest {
    entityId?: string;
}

export interface ApiRelationshipTypeListPostRequest {
    apiRequest?: ApiRequest;
}

export interface ApiRelationshipTypePostRequest {
    relationshipTypeConstruct?: RelationshipTypeConstruct;
}

export interface ApiRelationshipTypeRestorePostRequest {
    entityId?: string;
}

/**
 * RelationshipTypeApi - interface
 * 
 * @export
 * @interface RelationshipTypeApiInterface
 */
export interface RelationshipTypeApiInterface {
    /**
     * 
     * @param {string} [entityId] 
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     * @memberof RelationshipTypeApiInterface
     */
    apiRelationshipTypeDeleteRaw(requestParameters: ApiRelationshipTypeDeleteRequest): Promise<runtime.ApiResponse<void>>;

    /**
     */
    apiRelationshipTypeDelete(requestParameters: ApiRelationshipTypeDeleteRequest): Promise<void>;

    /**
     * 
     * @param {ApiRequest} [apiRequest] 
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     * @memberof RelationshipTypeApiInterface
     */
    apiRelationshipTypeListPostRaw(requestParameters: ApiRelationshipTypeListPostRequest): Promise<runtime.ApiResponse<IRelationshipTypeIStorableIEnumerableIDataResponse>>;

    /**
     */
    apiRelationshipTypeListPost(requestParameters: ApiRelationshipTypeListPostRequest): Promise<IRelationshipTypeIStorableIEnumerableIDataResponse>;

    /**
     * 
     * @param {RelationshipTypeConstruct} [relationshipTypeConstruct] 
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     * @memberof RelationshipTypeApiInterface
     */
    apiRelationshipTypePostRaw(requestParameters: ApiRelationshipTypePostRequest): Promise<runtime.ApiResponse<IRelationshipTypeIStorableIDataResponse>>;

    /**
     */
    apiRelationshipTypePost(requestParameters: ApiRelationshipTypePostRequest): Promise<IRelationshipTypeIStorableIDataResponse>;

    /**
     * 
     * @param {string} [entityId] 
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     * @memberof RelationshipTypeApiInterface
     */
    apiRelationshipTypeRestorePostRaw(requestParameters: ApiRelationshipTypeRestorePostRequest): Promise<runtime.ApiResponse<void>>;

    /**
     */
    apiRelationshipTypeRestorePost(requestParameters: ApiRelationshipTypeRestorePostRequest): Promise<void>;

}

/**
 * 
 */
export class RelationshipTypeApi extends runtime.BaseAPI implements RelationshipTypeApiInterface {

    /**
     */
    async apiRelationshipTypeDeleteRaw(requestParameters: ApiRelationshipTypeDeleteRequest): Promise<runtime.ApiResponse<void>> {
        const queryParameters: any = {};

        if (requestParameters.entityId !== undefined) {
            queryParameters['entityId'] = requestParameters.entityId;
        }

        const headerParameters: runtime.HTTPHeaders = {};

        const response = await this.request({
            path: `/api/RelationshipType`,
            method: 'DELETE',
            headers: headerParameters,
            query: queryParameters,
        });

        return new runtime.VoidApiResponse(response);
    }

    /**
     */
    async apiRelationshipTypeDelete(requestParameters: ApiRelationshipTypeDeleteRequest): Promise<void> {
        await this.apiRelationshipTypeDeleteRaw(requestParameters);
    }

    /**
     */
    async apiRelationshipTypeListPostRaw(requestParameters: ApiRelationshipTypeListPostRequest): Promise<runtime.ApiResponse<IRelationshipTypeIStorableIEnumerableIDataResponse>> {
        const queryParameters: any = {};

        const headerParameters: runtime.HTTPHeaders = {};

        headerParameters['Content-Type'] = 'application/json-patch+json';

        const response = await this.request({
            path: `/api/RelationshipType/List`,
            method: 'POST',
            headers: headerParameters,
            query: queryParameters,
            body: ApiRequestToJSON(requestParameters.apiRequest),
        });

        return new runtime.JSONApiResponse(response, (jsonValue) => IRelationshipTypeIStorableIEnumerableIDataResponseFromJSON(jsonValue));
    }

    /**
     */
    async apiRelationshipTypeListPost(requestParameters: ApiRelationshipTypeListPostRequest): Promise<IRelationshipTypeIStorableIEnumerableIDataResponse> {
        const response = await this.apiRelationshipTypeListPostRaw(requestParameters);
        return await response.value();
    }

    /**
     */
    async apiRelationshipTypePostRaw(requestParameters: ApiRelationshipTypePostRequest): Promise<runtime.ApiResponse<IRelationshipTypeIStorableIDataResponse>> {
        const queryParameters: any = {};

        const headerParameters: runtime.HTTPHeaders = {};

        headerParameters['Content-Type'] = 'application/json-patch+json';

        const response = await this.request({
            path: `/api/RelationshipType`,
            method: 'POST',
            headers: headerParameters,
            query: queryParameters,
            body: RelationshipTypeConstructToJSON(requestParameters.relationshipTypeConstruct),
        });

        return new runtime.JSONApiResponse(response, (jsonValue) => IRelationshipTypeIStorableIDataResponseFromJSON(jsonValue));
    }

    /**
     */
    async apiRelationshipTypePost(requestParameters: ApiRelationshipTypePostRequest): Promise<IRelationshipTypeIStorableIDataResponse> {
        const response = await this.apiRelationshipTypePostRaw(requestParameters);
        return await response.value();
    }

    /**
     */
    async apiRelationshipTypeRestorePostRaw(requestParameters: ApiRelationshipTypeRestorePostRequest): Promise<runtime.ApiResponse<void>> {
        const queryParameters: any = {};

        if (requestParameters.entityId !== undefined) {
            queryParameters['entityId'] = requestParameters.entityId;
        }

        const headerParameters: runtime.HTTPHeaders = {};

        const response = await this.request({
            path: `/api/RelationshipType/Restore`,
            method: 'POST',
            headers: headerParameters,
            query: queryParameters,
        });

        return new runtime.VoidApiResponse(response);
    }

    /**
     */
    async apiRelationshipTypeRestorePost(requestParameters: ApiRelationshipTypeRestorePostRequest): Promise<void> {
        await this.apiRelationshipTypeRestorePostRaw(requestParameters);
    }

}
