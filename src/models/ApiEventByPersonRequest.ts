/* tslint:disable */
/* eslint-disable */
/**
 * ChiRhos API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: v1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime';
/**
 * 
 * @export
 * @interface ApiEventByPersonRequest
 */
export interface ApiEventByPersonRequest {
    /**
     * 
     * @type {string}
     * @memberof ApiEventByPersonRequest
     */
    eventEntityId?: string;
    /**
     * 
     * @type {string}
     * @memberof ApiEventByPersonRequest
     */
    uniqueCode?: string | null;
}

export function ApiEventByPersonRequestFromJSON(json: any): ApiEventByPersonRequest {
    return ApiEventByPersonRequestFromJSONTyped(json, false);
}

export function ApiEventByPersonRequestFromJSONTyped(json: any, ignoreDiscriminator: boolean): ApiEventByPersonRequest {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        
        'eventEntityId': !exists(json, 'eventEntityId') ? undefined : json['eventEntityId'],
        'uniqueCode': !exists(json, 'uniqueCode') ? undefined : json['uniqueCode'],
    };
}

export function ApiEventByPersonRequestToJSON(value?: ApiEventByPersonRequest | null): any {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        
        'eventEntityId': value.eventEntityId,
        'uniqueCode': value.uniqueCode,
    };
}


