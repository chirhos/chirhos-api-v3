/* tslint:disable */
/* eslint-disable */
/**
 * ChiRhos API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: v1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime';
import {
    RelationshipContainer,
    RelationshipContainerFromJSON,
    RelationshipContainerFromJSONTyped,
    RelationshipContainerToJSON,
} from './';

/**
 * 
 * @export
 * @interface RelationshipContainerIEnumerableIDataResponse
 */
export interface RelationshipContainerIEnumerableIDataResponse {
    /**
     * 
     * @type {boolean}
     * @memberof RelationshipContainerIEnumerableIDataResponse
     */
    readonly isOk?: boolean;
    /**
     * 
     * @type {string}
     * @memberof RelationshipContainerIEnumerableIDataResponse
     */
    readonly message?: string | null;
    /**
     * 
     * @type {Array<RelationshipContainer>}
     * @memberof RelationshipContainerIEnumerableIDataResponse
     */
    readonly data?: Array<RelationshipContainer> | null;
    /**
     * 
     * @type {number}
     * @memberof RelationshipContainerIEnumerableIDataResponse
     */
    executionMilliseconds?: number;
    /**
     * 
     * @type {number}
     * @memberof RelationshipContainerIEnumerableIDataResponse
     */
    totalMilliseconds?: number;
    /**
     * 
     * @type {number}
     * @memberof RelationshipContainerIEnumerableIDataResponse
     */
    remoteCallMilliseconds?: number;
    /**
     * 
     * @type {string}
     * @memberof RelationshipContainerIEnumerableIDataResponse
     */
    executor?: string;
}

export function RelationshipContainerIEnumerableIDataResponseFromJSON(json: any): RelationshipContainerIEnumerableIDataResponse {
    return RelationshipContainerIEnumerableIDataResponseFromJSONTyped(json, false);
}

export function RelationshipContainerIEnumerableIDataResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): RelationshipContainerIEnumerableIDataResponse {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        
        'isOk': !exists(json, 'isOk') ? undefined : json['isOk'],
        'message': !exists(json, 'message') ? undefined : json['message'],
        'data': !exists(json, 'data') ? undefined : (json['data'] === null ? null : (json['data'] as Array<any>).map(RelationshipContainerFromJSON)),
        'executionMilliseconds': !exists(json, 'executionMilliseconds') ? undefined : json['executionMilliseconds'],
        'totalMilliseconds': !exists(json, 'totalMilliseconds') ? undefined : json['totalMilliseconds'],
        'remoteCallMilliseconds': !exists(json, 'remoteCallMilliseconds') ? undefined : json['remoteCallMilliseconds'],
        'executor': !exists(json, 'executor') ? undefined : json['executor'],
    };
}

export function RelationshipContainerIEnumerableIDataResponseToJSON(value?: RelationshipContainerIEnumerableIDataResponse | null): any {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        
        'executionMilliseconds': value.executionMilliseconds,
        'totalMilliseconds': value.totalMilliseconds,
        'remoteCallMilliseconds': value.remoteCallMilliseconds,
        'executor': value.executor,
    };
}


