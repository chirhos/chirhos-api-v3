/* tslint:disable */
/* eslint-disable */
/**
 * ChiRhos API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: v1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime';
import {
    Address,
    AddressFromJSON,
    AddressFromJSONTyped,
    AddressToJSON,
    Integration,
    IntegrationFromJSON,
    IntegrationFromJSONTyped,
    IntegrationToJSON,
    Limit,
    LimitFromJSON,
    LimitFromJSONTyped,
    LimitToJSON,
    Recurrence,
    RecurrenceFromJSON,
    RecurrenceFromJSONTyped,
    RecurrenceToJSON,
} from './';

/**
 * 
 * @export
 * @interface EventSeriesConstruct
 */
export interface EventSeriesConstruct {
    /**
     * 
     * @type {Recurrence}
     * @memberof EventSeriesConstruct
     */
    recurrence?: Recurrence;
    /**
     * 
     * @type {string}
     * @memberof EventSeriesConstruct
     */
    type?: string;
    /**
     * 
     * @type {string}
     * @memberof EventSeriesConstruct
     */
    belongsTo?: string;
    /**
     * 
     * @type {string}
     * @memberof EventSeriesConstruct
     */
    name?: string | null;
    /**
     * 
     * @type {Date}
     * @memberof EventSeriesConstruct
     */
    start?: Date;
    /**
     * 
     * @type {number}
     * @memberof EventSeriesConstruct
     */
    minutesDuration?: number;
    /**
     * 
     * @type {Limit}
     * @memberof EventSeriesConstruct
     */
    limit?: Limit;
    /**
     * 
     * @type {string}
     * @memberof EventSeriesConstruct
     */
    readonly organizerPersonEntityId?: string;
    /**
     * 
     * @type {Address}
     * @memberof EventSeriesConstruct
     */
    address?: Address;
    /**
     * 
     * @type {string}
     * @memberof EventSeriesConstruct
     */
    description?: string | null;
    /**
     * 
     * @type {string}
     * @memberof EventSeriesConstruct
     */
    url?: string | null;
    /**
     * 
     * @type {boolean}
     * @memberof EventSeriesConstruct
     */
    registrationOnly?: boolean;
    /**
     * 
     * @type {Date}
     * @memberof EventSeriesConstruct
     */
    registrationOpen?: Date | null;
    /**
     * 
     * @type {number}
     * @memberof EventSeriesConstruct
     */
    registrationOpenDaysBefore?: number | null;
    /**
     * 
     * @type {Date}
     * @memberof EventSeriesConstruct
     */
    registrationClose?: Date | null;
    /**
     * 
     * @type {number}
     * @memberof EventSeriesConstruct
     */
    registrationCloseDaysBefore?: number | null;
    /**
     * 
     * @type {Date}
     * @memberof EventSeriesConstruct
     */
    checkInOpen?: Date | null;
    /**
     * 
     * @type {number}
     * @memberof EventSeriesConstruct
     */
    checkInOpenHoursBefore?: number | null;
    /**
     * 
     * @type {Date}
     * @memberof EventSeriesConstruct
     */
    checkInClose?: Date | null;
    /**
     * 
     * @type {number}
     * @memberof EventSeriesConstruct
     */
    checkInCloseHoursAfter?: number | null;
    /**
     * 
     * @type {string}
     * @memberof EventSeriesConstruct
     */
    entityId?: string;
    /**
     * 
     * @type {{ [key: string]: object; }}
     * @memberof EventSeriesConstruct
     */
    properties?: { [key: string]: object; } | null;
    /**
     * 
     * @type {Integration}
     * @memberof EventSeriesConstruct
     */
    integration?: Integration;
}

export function EventSeriesConstructFromJSON(json: any): EventSeriesConstruct {
    return EventSeriesConstructFromJSONTyped(json, false);
}

export function EventSeriesConstructFromJSONTyped(json: any, ignoreDiscriminator: boolean): EventSeriesConstruct {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        
        'recurrence': !exists(json, 'recurrence') ? undefined : RecurrenceFromJSON(json['recurrence']),
        'type': !exists(json, 'type') ? undefined : json['type'],
        'belongsTo': !exists(json, 'belongsTo') ? undefined : json['belongsTo'],
        'name': !exists(json, 'name') ? undefined : json['name'],
        'start': !exists(json, 'start') ? undefined : (new Date(json['start'])),
        'minutesDuration': !exists(json, 'minutesDuration') ? undefined : json['minutesDuration'],
        'limit': !exists(json, 'limit') ? undefined : LimitFromJSON(json['limit']),
        'organizerPersonEntityId': !exists(json, 'organizerPersonEntityId') ? undefined : json['organizerPersonEntityId'],
        'address': !exists(json, 'address') ? undefined : AddressFromJSON(json['address']),
        'description': !exists(json, 'description') ? undefined : json['description'],
        'url': !exists(json, 'url') ? undefined : json['url'],
        'registrationOnly': !exists(json, 'registrationOnly') ? undefined : json['registrationOnly'],
        'registrationOpen': !exists(json, 'registrationOpen') ? undefined : (json['registrationOpen'] === null ? null : new Date(json['registrationOpen'])),
        'registrationOpenDaysBefore': !exists(json, 'registrationOpenDaysBefore') ? undefined : json['registrationOpenDaysBefore'],
        'registrationClose': !exists(json, 'registrationClose') ? undefined : (json['registrationClose'] === null ? null : new Date(json['registrationClose'])),
        'registrationCloseDaysBefore': !exists(json, 'registrationCloseDaysBefore') ? undefined : json['registrationCloseDaysBefore'],
        'checkInOpen': !exists(json, 'checkInOpen') ? undefined : (json['checkInOpen'] === null ? null : new Date(json['checkInOpen'])),
        'checkInOpenHoursBefore': !exists(json, 'checkInOpenHoursBefore') ? undefined : json['checkInOpenHoursBefore'],
        'checkInClose': !exists(json, 'checkInClose') ? undefined : (json['checkInClose'] === null ? null : new Date(json['checkInClose'])),
        'checkInCloseHoursAfter': !exists(json, 'checkInCloseHoursAfter') ? undefined : json['checkInCloseHoursAfter'],
        'entityId': !exists(json, 'entityId') ? undefined : json['entityId'],
        'properties': !exists(json, 'properties') ? undefined : json['properties'],
        'integration': !exists(json, 'integration') ? undefined : IntegrationFromJSON(json['integration']),
    };
}

export function EventSeriesConstructToJSON(value?: EventSeriesConstruct | null): any {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        
        'recurrence': RecurrenceToJSON(value.recurrence),
        'type': value.type,
        'belongsTo': value.belongsTo,
        'name': value.name,
        'start': value.start === undefined ? undefined : (value.start.toISOString()),
        'minutesDuration': value.minutesDuration,
        'limit': LimitToJSON(value.limit),
        'address': AddressToJSON(value.address),
        'description': value.description,
        'url': value.url,
        'registrationOnly': value.registrationOnly,
        'registrationOpen': value.registrationOpen === undefined ? undefined : (value.registrationOpen === null ? null : value.registrationOpen.toISOString()),
        'registrationOpenDaysBefore': value.registrationOpenDaysBefore,
        'registrationClose': value.registrationClose === undefined ? undefined : (value.registrationClose === null ? null : value.registrationClose.toISOString()),
        'registrationCloseDaysBefore': value.registrationCloseDaysBefore,
        'checkInOpen': value.checkInOpen === undefined ? undefined : (value.checkInOpen === null ? null : value.checkInOpen.toISOString()),
        'checkInOpenHoursBefore': value.checkInOpenHoursBefore,
        'checkInClose': value.checkInClose === undefined ? undefined : (value.checkInClose === null ? null : value.checkInClose.toISOString()),
        'checkInCloseHoursAfter': value.checkInCloseHoursAfter,
        'entityId': value.entityId,
        'properties': value.properties,
        'integration': IntegrationToJSON(value.integration),
    };
}


