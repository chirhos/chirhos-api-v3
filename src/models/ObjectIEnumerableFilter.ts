/* tslint:disable */
/* eslint-disable */
/**
 * ChiRhos API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: v1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime';
/**
 * 
 * @export
 * @interface ObjectIEnumerableFilter
 */
export interface ObjectIEnumerableFilter {
    /**
     * 
     * @type {string}
     * @memberof ObjectIEnumerableFilter
     */
    property: string;
    /**
     * 
     * @type {boolean}
     * @memberof ObjectIEnumerableFilter
     */
    not?: boolean;
    /**
     * 
     * @type {Array<object>}
     * @memberof ObjectIEnumerableFilter
     */
    value?: Array<object> | null;
}

export function ObjectIEnumerableFilterFromJSON(json: any): ObjectIEnumerableFilter {
    return ObjectIEnumerableFilterFromJSONTyped(json, false);
}

export function ObjectIEnumerableFilterFromJSONTyped(json: any, ignoreDiscriminator: boolean): ObjectIEnumerableFilter {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        
        'property': json['property'],
        'not': !exists(json, 'not') ? undefined : json['not'],
        'value': !exists(json, 'value') ? undefined : json['value'],
    };
}

export function ObjectIEnumerableFilterToJSON(value?: ObjectIEnumerableFilter | null): any {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        
        'property': value.property,
        'not': value.not,
        'value': value.value,
    };
}


