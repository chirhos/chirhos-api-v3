/* tslint:disable */
/* eslint-disable */
/**
 * ChiRhos API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: v1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime';
import {
    Favourite,
    FavouriteFromJSON,
    FavouriteFromJSONTyped,
    FavouriteToJSON,
    Integration,
    IntegrationFromJSON,
    IntegrationFromJSONTyped,
    IntegrationToJSON,
    PersonStatusByTenant,
    PersonStatusByTenantFromJSON,
    PersonStatusByTenantFromJSONTyped,
    PersonStatusByTenantToJSON,
} from './';

/**
 * 
 * @export
 * @interface IUser
 */
export interface IUser {
    /**
     * 
     * @type {string}
     * @memberof IUser
     */
    readonly userId: string;
    /**
     * 
     * @type {boolean}
     * @memberof IUser
     */
    isChiRhosOwner?: boolean;
    /**
     * 
     * @type {Array<PersonStatusByTenant>}
     * @memberof IUser
     */
    personStatusByTenant?: Array<PersonStatusByTenant> | null;
    /**
     * 
     * @type {Array<Favourite>}
     * @memberof IUser
     */
    readonly favourites?: Array<Favourite> | null;
    /**
     * 
     * @type {string}
     * @memberof IUser
     */
    readonly entityId?: string;
    /**
     * 
     * @type {{ [key: string]: object; }}
     * @memberof IUser
     */
    properties?: { [key: string]: object; } | null;
    /**
     * 
     * @type {Integration}
     * @memberof IUser
     */
    integration?: Integration;
}

export function IUserFromJSON(json: any): IUser {
    return IUserFromJSONTyped(json, false);
}

export function IUserFromJSONTyped(json: any, ignoreDiscriminator: boolean): IUser {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        
        'userId': json['userId'],
        'isChiRhosOwner': !exists(json, 'isChiRhosOwner') ? undefined : json['isChiRhosOwner'],
        'personStatusByTenant': !exists(json, 'personStatusByTenant') ? undefined : (json['personStatusByTenant'] === null ? null : (json['personStatusByTenant'] as Array<any>).map(PersonStatusByTenantFromJSON)),
        'favourites': !exists(json, 'favourites') ? undefined : (json['favourites'] === null ? null : (json['favourites'] as Array<any>).map(FavouriteFromJSON)),
        'entityId': !exists(json, 'entityId') ? undefined : json['entityId'],
        'properties': !exists(json, 'properties') ? undefined : json['properties'],
        'integration': !exists(json, 'integration') ? undefined : IntegrationFromJSON(json['integration']),
    };
}

export function IUserToJSON(value?: IUser | null): any {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        
        'isChiRhosOwner': value.isChiRhosOwner,
        'personStatusByTenant': value.personStatusByTenant === undefined ? undefined : (value.personStatusByTenant === null ? null : (value.personStatusByTenant as Array<any>).map(PersonStatusByTenantToJSON)),
        'properties': value.properties,
        'integration': IntegrationToJSON(value.integration),
    };
}


