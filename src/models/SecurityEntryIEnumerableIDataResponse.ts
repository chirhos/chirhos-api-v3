/* tslint:disable */
/* eslint-disable */
/**
 * ChiRhos API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: v1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime';
import {
    SecurityEntry,
    SecurityEntryFromJSON,
    SecurityEntryFromJSONTyped,
    SecurityEntryToJSON,
} from './';

/**
 * 
 * @export
 * @interface SecurityEntryIEnumerableIDataResponse
 */
export interface SecurityEntryIEnumerableIDataResponse {
    /**
     * 
     * @type {boolean}
     * @memberof SecurityEntryIEnumerableIDataResponse
     */
    readonly isOk?: boolean;
    /**
     * 
     * @type {string}
     * @memberof SecurityEntryIEnumerableIDataResponse
     */
    readonly message?: string | null;
    /**
     * 
     * @type {Array<SecurityEntry>}
     * @memberof SecurityEntryIEnumerableIDataResponse
     */
    readonly data?: Array<SecurityEntry> | null;
    /**
     * 
     * @type {number}
     * @memberof SecurityEntryIEnumerableIDataResponse
     */
    executionMilliseconds?: number;
    /**
     * 
     * @type {number}
     * @memberof SecurityEntryIEnumerableIDataResponse
     */
    totalMilliseconds?: number;
    /**
     * 
     * @type {number}
     * @memberof SecurityEntryIEnumerableIDataResponse
     */
    remoteCallMilliseconds?: number;
    /**
     * 
     * @type {string}
     * @memberof SecurityEntryIEnumerableIDataResponse
     */
    executor?: string;
}

export function SecurityEntryIEnumerableIDataResponseFromJSON(json: any): SecurityEntryIEnumerableIDataResponse {
    return SecurityEntryIEnumerableIDataResponseFromJSONTyped(json, false);
}

export function SecurityEntryIEnumerableIDataResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): SecurityEntryIEnumerableIDataResponse {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        
        'isOk': !exists(json, 'isOk') ? undefined : json['isOk'],
        'message': !exists(json, 'message') ? undefined : json['message'],
        'data': !exists(json, 'data') ? undefined : (json['data'] === null ? null : (json['data'] as Array<any>).map(SecurityEntryFromJSON)),
        'executionMilliseconds': !exists(json, 'executionMilliseconds') ? undefined : json['executionMilliseconds'],
        'totalMilliseconds': !exists(json, 'totalMilliseconds') ? undefined : json['totalMilliseconds'],
        'remoteCallMilliseconds': !exists(json, 'remoteCallMilliseconds') ? undefined : json['remoteCallMilliseconds'],
        'executor': !exists(json, 'executor') ? undefined : json['executor'],
    };
}

export function SecurityEntryIEnumerableIDataResponseToJSON(value?: SecurityEntryIEnumerableIDataResponse | null): any {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        
        'executionMilliseconds': value.executionMilliseconds,
        'totalMilliseconds': value.totalMilliseconds,
        'remoteCallMilliseconds': value.remoteCallMilliseconds,
        'executor': value.executor,
    };
}


