/* tslint:disable */
/* eslint-disable */
/**
 * ChiRhos API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: v1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime';
/**
 * 
 * @export
 * @interface GroupUsageResponse
 */
export interface GroupUsageResponse {
    /**
     * 
     * @type {string}
     * @memberof GroupUsageResponse
     */
    groupName?: string | null;
    /**
     * 
     * @type {string}
     * @memberof GroupUsageResponse
     */
    groupEntityId?: string;
    /**
     * 
     * @type {number}
     * @memberof GroupUsageResponse
     */
    groupStorageUsed?: number;
    /**
     * 
     * @type {number}
     * @memberof GroupUsageResponse
     */
    allProfiles?: number;
    /**
     * 
     * @type {number}
     * @memberof GroupUsageResponse
     */
    activeProfiles?: number;
    /**
     * 
     * @type {number}
     * @memberof GroupUsageResponse
     */
    totalStorageUsed?: number;
    /**
     * 
     * @type {number}
     * @memberof GroupUsageResponse
     */
    executionMilliseconds?: number;
    /**
     * 
     * @type {number}
     * @memberof GroupUsageResponse
     */
    remoteCallMilliseconds?: number;
    /**
     * 
     * @type {number}
     * @memberof GroupUsageResponse
     */
    totalMilliseconds?: number;
    /**
     * 
     * @type {string}
     * @memberof GroupUsageResponse
     */
    executor?: string;
}

export function GroupUsageResponseFromJSON(json: any): GroupUsageResponse {
    return GroupUsageResponseFromJSONTyped(json, false);
}

export function GroupUsageResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): GroupUsageResponse {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        
        'groupName': !exists(json, 'groupName') ? undefined : json['groupName'],
        'groupEntityId': !exists(json, 'groupEntityId') ? undefined : json['groupEntityId'],
        'groupStorageUsed': !exists(json, 'groupStorageUsed') ? undefined : json['groupStorageUsed'],
        'allProfiles': !exists(json, 'allProfiles') ? undefined : json['allProfiles'],
        'activeProfiles': !exists(json, 'activeProfiles') ? undefined : json['activeProfiles'],
        'totalStorageUsed': !exists(json, 'totalStorageUsed') ? undefined : json['totalStorageUsed'],
        'executionMilliseconds': !exists(json, 'executionMilliseconds') ? undefined : json['executionMilliseconds'],
        'remoteCallMilliseconds': !exists(json, 'remoteCallMilliseconds') ? undefined : json['remoteCallMilliseconds'],
        'totalMilliseconds': !exists(json, 'totalMilliseconds') ? undefined : json['totalMilliseconds'],
        'executor': !exists(json, 'executor') ? undefined : json['executor'],
    };
}

export function GroupUsageResponseToJSON(value?: GroupUsageResponse | null): any {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        
        'groupName': value.groupName,
        'groupEntityId': value.groupEntityId,
        'groupStorageUsed': value.groupStorageUsed,
        'allProfiles': value.allProfiles,
        'activeProfiles': value.activeProfiles,
        'totalStorageUsed': value.totalStorageUsed,
        'executionMilliseconds': value.executionMilliseconds,
        'remoteCallMilliseconds': value.remoteCallMilliseconds,
        'totalMilliseconds': value.totalMilliseconds,
        'executor': value.executor,
    };
}


