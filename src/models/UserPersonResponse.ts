/* tslint:disable */
/* eslint-disable */
/**
 * ChiRhos API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: v1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime';
import {
    IPerson,
    IPersonFromJSON,
    IPersonFromJSONTyped,
    IPersonToJSON,
    IUser,
    IUserFromJSON,
    IUserFromJSONTyped,
    IUserToJSON,
} from './';

/**
 * 
 * @export
 * @interface UserPersonResponse
 */
export interface UserPersonResponse {
    /**
     * 
     * @type {IUser}
     * @memberof UserPersonResponse
     */
    user?: IUser;
    /**
     * 
     * @type {IPerson}
     * @memberof UserPersonResponse
     */
    person?: IPerson;
}

export function UserPersonResponseFromJSON(json: any): UserPersonResponse {
    return UserPersonResponseFromJSONTyped(json, false);
}

export function UserPersonResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): UserPersonResponse {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        
        'user': !exists(json, 'user') ? undefined : IUserFromJSON(json['user']),
        'person': !exists(json, 'person') ? undefined : IPersonFromJSON(json['person']),
    };
}

export function UserPersonResponseToJSON(value?: UserPersonResponse | null): any {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        
        'user': IUserToJSON(value.user),
        'person': IPersonToJSON(value.person),
    };
}


