/* tslint:disable */
/* eslint-disable */
/**
 * ChiRhos API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: v1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime';
import {
    Feed,
    FeedFromJSON,
    FeedFromJSONTyped,
    FeedToJSON,
    Integration,
    IntegrationFromJSON,
    IntegrationFromJSONTyped,
    IntegrationToJSON,
    PropertyPreset,
    PropertyPresetFromJSON,
    PropertyPresetFromJSONTyped,
    PropertyPresetToJSON,
} from './';

/**
 * 
 * @export
 * @interface ITenant
 */
export interface ITenant {
    /**
     * 
     * @type {{ [key: string]: PropertyPreset; }}
     * @memberof ITenant
     */
    readonly personPropertyPresets?: { [key: string]: PropertyPreset; } | null;
    /**
     * 
     * @type {Array<Feed>}
     * @memberof ITenant
     */
    feeds?: Array<Feed> | null;
    /**
     * 
     * @type {Array<string>}
     * @memberof ITenant
     */
    highlightGroups?: Array<string> | null;
    /**
     * 
     * @type {string}
     * @memberof ITenant
     */
    readonly colorPrimary?: string | null;
    /**
     * 
     * @type {string}
     * @memberof ITenant
     */
    readonly colorSecondary?: string | null;
    /**
     * 
     * @type {string}
     * @memberof ITenant
     */
    readonly colorTernary?: string | null;
    /**
     * 
     * @type {string}
     * @memberof ITenant
     */
    readonly logoUrlLightTheme?: string | null;
    /**
     * 
     * @type {string}
     * @memberof ITenant
     */
    readonly logoUrlDarkTheme?: string | null;
    /**
     * 
     * @type {string}
     * @memberof ITenant
     */
    readonly mobileAppName?: string | null;
    /**
     * 
     * @type {string}
     * @memberof ITenant
     */
    readonly disclaimer?: string | null;
    /**
     * 
     * @type {string}
     * @memberof ITenant
     */
    readonly entityId?: string;
    /**
     * 
     * @type {{ [key: string]: object; }}
     * @memberof ITenant
     */
    properties?: { [key: string]: object; } | null;
    /**
     * 
     * @type {Integration}
     * @memberof ITenant
     */
    integration?: Integration;
}

export function ITenantFromJSON(json: any): ITenant {
    return ITenantFromJSONTyped(json, false);
}

export function ITenantFromJSONTyped(json: any, ignoreDiscriminator: boolean): ITenant {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        
        'personPropertyPresets': !exists(json, 'personPropertyPresets') ? undefined : (json['personPropertyPresets'] === null ? null : mapValues(json['personPropertyPresets'], PropertyPresetFromJSON)),
        'feeds': !exists(json, 'feeds') ? undefined : (json['feeds'] === null ? null : (json['feeds'] as Array<any>).map(FeedFromJSON)),
        'highlightGroups': !exists(json, 'highlightGroups') ? undefined : json['highlightGroups'],
        'colorPrimary': !exists(json, 'colorPrimary') ? undefined : json['colorPrimary'],
        'colorSecondary': !exists(json, 'colorSecondary') ? undefined : json['colorSecondary'],
        'colorTernary': !exists(json, 'colorTernary') ? undefined : json['colorTernary'],
        'logoUrlLightTheme': !exists(json, 'logoUrlLightTheme') ? undefined : json['logoUrlLightTheme'],
        'logoUrlDarkTheme': !exists(json, 'logoUrlDarkTheme') ? undefined : json['logoUrlDarkTheme'],
        'mobileAppName': !exists(json, 'mobileAppName') ? undefined : json['mobileAppName'],
        'disclaimer': !exists(json, 'disclaimer') ? undefined : json['disclaimer'],
        'entityId': !exists(json, 'entityId') ? undefined : json['entityId'],
        'properties': !exists(json, 'properties') ? undefined : json['properties'],
        'integration': !exists(json, 'integration') ? undefined : IntegrationFromJSON(json['integration']),
    };
}

export function ITenantToJSON(value?: ITenant | null): any {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        
        'feeds': value.feeds === undefined ? undefined : (value.feeds === null ? null : (value.feeds as Array<any>).map(FeedToJSON)),
        'highlightGroups': value.highlightGroups,
        'properties': value.properties,
        'integration': IntegrationToJSON(value.integration),
    };
}


