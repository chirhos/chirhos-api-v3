/* tslint:disable */
/* eslint-disable */
/**
 * ChiRhos API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: v1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime';
import {
    UserSummary,
    UserSummaryFromJSON,
    UserSummaryFromJSONTyped,
    UserSummaryToJSON,
} from './';

/**
 * 
 * @export
 * @interface UserSummaryIEnumerableIDataResponse
 */
export interface UserSummaryIEnumerableIDataResponse {
    /**
     * 
     * @type {boolean}
     * @memberof UserSummaryIEnumerableIDataResponse
     */
    readonly isOk?: boolean;
    /**
     * 
     * @type {string}
     * @memberof UserSummaryIEnumerableIDataResponse
     */
    readonly message?: string | null;
    /**
     * 
     * @type {Array<UserSummary>}
     * @memberof UserSummaryIEnumerableIDataResponse
     */
    readonly data?: Array<UserSummary> | null;
    /**
     * 
     * @type {number}
     * @memberof UserSummaryIEnumerableIDataResponse
     */
    executionMilliseconds?: number;
    /**
     * 
     * @type {number}
     * @memberof UserSummaryIEnumerableIDataResponse
     */
    totalMilliseconds?: number;
    /**
     * 
     * @type {number}
     * @memberof UserSummaryIEnumerableIDataResponse
     */
    remoteCallMilliseconds?: number;
    /**
     * 
     * @type {string}
     * @memberof UserSummaryIEnumerableIDataResponse
     */
    executor?: string;
}

export function UserSummaryIEnumerableIDataResponseFromJSON(json: any): UserSummaryIEnumerableIDataResponse {
    return UserSummaryIEnumerableIDataResponseFromJSONTyped(json, false);
}

export function UserSummaryIEnumerableIDataResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): UserSummaryIEnumerableIDataResponse {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        
        'isOk': !exists(json, 'isOk') ? undefined : json['isOk'],
        'message': !exists(json, 'message') ? undefined : json['message'],
        'data': !exists(json, 'data') ? undefined : (json['data'] === null ? null : (json['data'] as Array<any>).map(UserSummaryFromJSON)),
        'executionMilliseconds': !exists(json, 'executionMilliseconds') ? undefined : json['executionMilliseconds'],
        'totalMilliseconds': !exists(json, 'totalMilliseconds') ? undefined : json['totalMilliseconds'],
        'remoteCallMilliseconds': !exists(json, 'remoteCallMilliseconds') ? undefined : json['remoteCallMilliseconds'],
        'executor': !exists(json, 'executor') ? undefined : json['executor'],
    };
}

export function UserSummaryIEnumerableIDataResponseToJSON(value?: UserSummaryIEnumerableIDataResponse | null): any {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        
        'executionMilliseconds': value.executionMilliseconds,
        'totalMilliseconds': value.totalMilliseconds,
        'remoteCallMilliseconds': value.remoteCallMilliseconds,
        'executor': value.executor,
    };
}


