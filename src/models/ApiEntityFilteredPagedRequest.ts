/* tslint:disable */
/* eslint-disable */
/**
 * ChiRhos API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: v1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime';
import {
    FilterSet,
    FilterSetFromJSON,
    FilterSetFromJSONTyped,
    FilterSetToJSON,
    Sort,
    SortFromJSON,
    SortFromJSONTyped,
    SortToJSON,
} from './';

/**
 * 
 * @export
 * @interface ApiEntityFilteredPagedRequest
 */
export interface ApiEntityFilteredPagedRequest {
    /**
     * 
     * @type {string}
     * @memberof ApiEntityFilteredPagedRequest
     */
    entityId: string;
    /**
     * 
     * @type {FilterSet}
     * @memberof ApiEntityFilteredPagedRequest
     */
    filters?: FilterSet;
    /**
     * 
     * @type {number}
     * @memberof ApiEntityFilteredPagedRequest
     */
    page?: number;
    /**
     * 
     * @type {number}
     * @memberof ApiEntityFilteredPagedRequest
     */
    pageSize?: number;
    /**
     * 
     * @type {boolean}
     * @memberof ApiEntityFilteredPagedRequest
     */
    includeDeleted?: boolean;
    /**
     * 
     * @type {boolean}
     * @memberof ApiEntityFilteredPagedRequest
     */
    requestUnpaged?: boolean;
    /**
     * 
     * @type {Sort}
     * @memberof ApiEntityFilteredPagedRequest
     */
    sort?: Sort;
    /**
     * 
     * @type {Date}
     * @memberof ApiEntityFilteredPagedRequest
     */
    pointInTime?: Date | null;
}

export function ApiEntityFilteredPagedRequestFromJSON(json: any): ApiEntityFilteredPagedRequest {
    return ApiEntityFilteredPagedRequestFromJSONTyped(json, false);
}

export function ApiEntityFilteredPagedRequestFromJSONTyped(json: any, ignoreDiscriminator: boolean): ApiEntityFilteredPagedRequest {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        
        'entityId': json['entityId'],
        'filters': !exists(json, 'filters') ? undefined : FilterSetFromJSON(json['filters']),
        'page': !exists(json, 'page') ? undefined : json['page'],
        'pageSize': !exists(json, 'pageSize') ? undefined : json['pageSize'],
        'includeDeleted': !exists(json, 'includeDeleted') ? undefined : json['includeDeleted'],
        'requestUnpaged': !exists(json, 'requestUnpaged') ? undefined : json['requestUnpaged'],
        'sort': !exists(json, 'sort') ? undefined : SortFromJSON(json['sort']),
        'pointInTime': !exists(json, 'pointInTime') ? undefined : (json['pointInTime'] === null ? null : new Date(json['pointInTime'])),
    };
}

export function ApiEntityFilteredPagedRequestToJSON(value?: ApiEntityFilteredPagedRequest | null): any {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        
        'entityId': value.entityId,
        'filters': FilterSetToJSON(value.filters),
        'page': value.page,
        'pageSize': value.pageSize,
        'includeDeleted': value.includeDeleted,
        'requestUnpaged': value.requestUnpaged,
        'sort': SortToJSON(value.sort),
        'pointInTime': value.pointInTime === undefined ? undefined : (value.pointInTime === null ? null : value.pointInTime.toISOString()),
    };
}


