/* tslint:disable */
/* eslint-disable */
/**
 * ChiRhos API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: v1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime';
import {
    Address,
    AddressFromJSON,
    AddressFromJSONTyped,
    AddressToJSON,
    Integration,
    IntegrationFromJSON,
    IntegrationFromJSONTyped,
    IntegrationToJSON,
    Limit,
    LimitFromJSON,
    LimitFromJSONTyped,
    LimitToJSON,
    Occurrence,
    OccurrenceFromJSON,
    OccurrenceFromJSONTyped,
    OccurrenceToJSON,
} from './';

/**
 * 
 * @export
 * @interface IEvent
 */
export interface IEvent {
    /**
     * 
     * @type {Occurrence}
     * @memberof IEvent
     */
    belongsToSeries?: Occurrence;
    /**
     * 
     * @type {{ [key: string]: number; }}
     * @memberof IEvent
     */
    readonly headCountsExpected?: { [key: string]: number; } | null;
    /**
     * 
     * @type {{ [key: string]: number; }}
     * @memberof IEvent
     */
    readonly headCountsActual?: { [key: string]: number; } | null;
    /**
     * 
     * @type {boolean}
     * @memberof IEvent
     */
    readonly isCancelled?: boolean;
    /**
     * 
     * @type {string}
     * @memberof IEvent
     */
    readonly feedbackNoteEntityId?: string;
    /**
     * 
     * @type {string}
     * @memberof IEvent
     */
    type?: string;
    /**
     * 
     * @type {string}
     * @memberof IEvent
     */
    readonly name?: string | null;
    /**
     * 
     * @type {Date}
     * @memberof IEvent
     */
    readonly start?: Date;
    /**
     * 
     * @type {number}
     * @memberof IEvent
     */
    readonly minutesDuration?: number;
    /**
     * 
     * @type {string}
     * @memberof IEvent
     */
    readonly organizerPersonEntityId?: string;
    /**
     * 
     * @type {Address}
     * @memberof IEvent
     */
    address?: Address;
    /**
     * 
     * @type {string}
     * @memberof IEvent
     */
    description?: string | null;
    /**
     * 
     * @type {string}
     * @memberof IEvent
     */
    url?: string | null;
    /**
     * 
     * @type {boolean}
     * @memberof IEvent
     */
    readonly registrationOnly?: boolean;
    /**
     * 
     * @type {string}
     * @memberof IEvent
     */
    readonly entityId?: string;
    /**
     * 
     * @type {{ [key: string]: object; }}
     * @memberof IEvent
     */
    properties?: { [key: string]: object; } | null;
    /**
     * 
     * @type {Integration}
     * @memberof IEvent
     */
    integration?: Integration;
    /**
     * 
     * @type {string}
     * @memberof IEvent
     */
    readonly belongsTo?: string;
    /**
     * 
     * @type {Limit}
     * @memberof IEvent
     */
    limit?: Limit;
    /**
     * 
     * @type {Date}
     * @memberof IEvent
     */
    readonly registrationOpen?: Date | null;
    /**
     * 
     * @type {number}
     * @memberof IEvent
     */
    readonly registrationOpenDaysBefore?: number | null;
    /**
     * 
     * @type {Date}
     * @memberof IEvent
     */
    readonly registrationClose?: Date | null;
    /**
     * 
     * @type {number}
     * @memberof IEvent
     */
    readonly registrationCloseDaysBefore?: number | null;
    /**
     * 
     * @type {Date}
     * @memberof IEvent
     */
    readonly checkInOpen?: Date | null;
    /**
     * 
     * @type {number}
     * @memberof IEvent
     */
    readonly checkInOpenHoursBefore?: number | null;
    /**
     * 
     * @type {Date}
     * @memberof IEvent
     */
    readonly checkInClose?: Date | null;
    /**
     * 
     * @type {number}
     * @memberof IEvent
     */
    readonly checkInCloseHoursAfter?: number | null;
    /**
     * 
     * @type {string}
     * @memberof IEvent
     */
    readonly uniqueCode?: string | null;
}

export function IEventFromJSON(json: any): IEvent {
    return IEventFromJSONTyped(json, false);
}

export function IEventFromJSONTyped(json: any, ignoreDiscriminator: boolean): IEvent {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        
        'belongsToSeries': !exists(json, 'belongsToSeries') ? undefined : OccurrenceFromJSON(json['belongsToSeries']),
        'headCountsExpected': !exists(json, 'headCountsExpected') ? undefined : json['headCountsExpected'],
        'headCountsActual': !exists(json, 'headCountsActual') ? undefined : json['headCountsActual'],
        'isCancelled': !exists(json, 'isCancelled') ? undefined : json['isCancelled'],
        'feedbackNoteEntityId': !exists(json, 'feedbackNoteEntityId') ? undefined : json['feedbackNoteEntityId'],
        'type': !exists(json, 'type') ? undefined : json['type'],
        'name': !exists(json, 'name') ? undefined : json['name'],
        'start': !exists(json, 'start') ? undefined : (new Date(json['start'])),
        'minutesDuration': !exists(json, 'minutesDuration') ? undefined : json['minutesDuration'],
        'organizerPersonEntityId': !exists(json, 'organizerPersonEntityId') ? undefined : json['organizerPersonEntityId'],
        'address': !exists(json, 'address') ? undefined : AddressFromJSON(json['address']),
        'description': !exists(json, 'description') ? undefined : json['description'],
        'url': !exists(json, 'url') ? undefined : json['url'],
        'registrationOnly': !exists(json, 'registrationOnly') ? undefined : json['registrationOnly'],
        'entityId': !exists(json, 'entityId') ? undefined : json['entityId'],
        'properties': !exists(json, 'properties') ? undefined : json['properties'],
        'integration': !exists(json, 'integration') ? undefined : IntegrationFromJSON(json['integration']),
        'belongsTo': !exists(json, 'belongsTo') ? undefined : json['belongsTo'],
        'limit': !exists(json, 'limit') ? undefined : LimitFromJSON(json['limit']),
        'registrationOpen': !exists(json, 'registrationOpen') ? undefined : (json['registrationOpen'] === null ? null : new Date(json['registrationOpen'])),
        'registrationOpenDaysBefore': !exists(json, 'registrationOpenDaysBefore') ? undefined : json['registrationOpenDaysBefore'],
        'registrationClose': !exists(json, 'registrationClose') ? undefined : (json['registrationClose'] === null ? null : new Date(json['registrationClose'])),
        'registrationCloseDaysBefore': !exists(json, 'registrationCloseDaysBefore') ? undefined : json['registrationCloseDaysBefore'],
        'checkInOpen': !exists(json, 'checkInOpen') ? undefined : (json['checkInOpen'] === null ? null : new Date(json['checkInOpen'])),
        'checkInOpenHoursBefore': !exists(json, 'checkInOpenHoursBefore') ? undefined : json['checkInOpenHoursBefore'],
        'checkInClose': !exists(json, 'checkInClose') ? undefined : (json['checkInClose'] === null ? null : new Date(json['checkInClose'])),
        'checkInCloseHoursAfter': !exists(json, 'checkInCloseHoursAfter') ? undefined : json['checkInCloseHoursAfter'],
        'uniqueCode': !exists(json, 'uniqueCode') ? undefined : json['uniqueCode'],
    };
}

export function IEventToJSON(value?: IEvent | null): any {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        
        'belongsToSeries': OccurrenceToJSON(value.belongsToSeries),
        'type': value.type,
        'address': AddressToJSON(value.address),
        'description': value.description,
        'url': value.url,
        'properties': value.properties,
        'integration': IntegrationToJSON(value.integration),
        'limit': LimitToJSON(value.limit),
    };
}


