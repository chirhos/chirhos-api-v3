/* tslint:disable */
/* eslint-disable */
/**
 * ChiRhos API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: v1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime';
import {
    ISubscription,
    ISubscriptionFromJSON,
    ISubscriptionFromJSONTyped,
    ISubscriptionToJSON,
} from './';

/**
 * 
 * @export
 * @interface ISubscriptionIStorable
 */
export interface ISubscriptionIStorable {
    /**
     * 
     * @type {ISubscription}
     * @memberof ISubscriptionIStorable
     */
    entity?: ISubscription;
    /**
     * 
     * @type {string}
     * @memberof ISubscriptionIStorable
     */
    readonly tenant: string;
    /**
     * 
     * @type {string}
     * @memberof ISubscriptionIStorable
     */
    readonly entityType: string;
    /**
     * 
     * @type {Date}
     * @memberof ISubscriptionIStorable
     */
    from?: Date;
    /**
     * 
     * @type {Date}
     * @memberof ISubscriptionIStorable
     */
    to?: Date | null;
    /**
     * 
     * @type {string}
     * @memberof ISubscriptionIStorable
     */
    modifiedByPerson?: string;
    /**
     * 
     * @type {boolean}
     * @memberof ISubscriptionIStorable
     */
    isDeleted?: boolean;
}

export function ISubscriptionIStorableFromJSON(json: any): ISubscriptionIStorable {
    return ISubscriptionIStorableFromJSONTyped(json, false);
}

export function ISubscriptionIStorableFromJSONTyped(json: any, ignoreDiscriminator: boolean): ISubscriptionIStorable {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        
        'entity': !exists(json, 'entity') ? undefined : ISubscriptionFromJSON(json['entity']),
        'tenant': json['tenant'],
        'entityType': json['entityType'],
        'from': !exists(json, 'from') ? undefined : (new Date(json['from'])),
        'to': !exists(json, 'to') ? undefined : (json['to'] === null ? null : new Date(json['to'])),
        'modifiedByPerson': !exists(json, 'modifiedByPerson') ? undefined : json['modifiedByPerson'],
        'isDeleted': !exists(json, 'isDeleted') ? undefined : json['isDeleted'],
    };
}

export function ISubscriptionIStorableToJSON(value?: ISubscriptionIStorable | null): any {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        
        'entity': ISubscriptionToJSON(value.entity),
        'from': value.from === undefined ? undefined : (value.from.toISOString()),
        'to': value.to === undefined ? undefined : (value.to === null ? null : value.to.toISOString()),
        'modifiedByPerson': value.modifiedByPerson,
        'isDeleted': value.isDeleted,
    };
}


