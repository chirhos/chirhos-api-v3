/* tslint:disable */
/* eslint-disable */
/**
 * ChiRhos API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: v1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime';
/**
 * 
 * @export
 * @interface ObjectFilter
 */
export interface ObjectFilter {
    /**
     * 
     * @type {string}
     * @memberof ObjectFilter
     */
    property: string;
    /**
     * 
     * @type {boolean}
     * @memberof ObjectFilter
     */
    not?: boolean;
    /**
     * 
     * @type {object}
     * @memberof ObjectFilter
     */
    value?: object | null;
}

export function ObjectFilterFromJSON(json: any): ObjectFilter {
    return ObjectFilterFromJSONTyped(json, false);
}

export function ObjectFilterFromJSONTyped(json: any, ignoreDiscriminator: boolean): ObjectFilter {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        
        'property': json['property'],
        'not': !exists(json, 'not') ? undefined : json['not'],
        'value': !exists(json, 'value') ? undefined : json['value'],
    };
}

export function ObjectFilterToJSON(value?: ObjectFilter | null): any {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        
        'property': value.property,
        'not': value.not,
        'value': value.value,
    };
}


