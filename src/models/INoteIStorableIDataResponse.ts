/* tslint:disable */
/* eslint-disable */
/**
 * ChiRhos API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: v1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime';
import {
    INoteIStorable,
    INoteIStorableFromJSON,
    INoteIStorableFromJSONTyped,
    INoteIStorableToJSON,
} from './';

/**
 * 
 * @export
 * @interface INoteIStorableIDataResponse
 */
export interface INoteIStorableIDataResponse {
    /**
     * 
     * @type {boolean}
     * @memberof INoteIStorableIDataResponse
     */
    readonly isOk?: boolean;
    /**
     * 
     * @type {string}
     * @memberof INoteIStorableIDataResponse
     */
    readonly message?: string | null;
    /**
     * 
     * @type {INoteIStorable}
     * @memberof INoteIStorableIDataResponse
     */
    data?: INoteIStorable;
    /**
     * 
     * @type {number}
     * @memberof INoteIStorableIDataResponse
     */
    executionMilliseconds?: number;
    /**
     * 
     * @type {number}
     * @memberof INoteIStorableIDataResponse
     */
    totalMilliseconds?: number;
    /**
     * 
     * @type {number}
     * @memberof INoteIStorableIDataResponse
     */
    remoteCallMilliseconds?: number;
    /**
     * 
     * @type {string}
     * @memberof INoteIStorableIDataResponse
     */
    executor?: string;
}

export function INoteIStorableIDataResponseFromJSON(json: any): INoteIStorableIDataResponse {
    return INoteIStorableIDataResponseFromJSONTyped(json, false);
}

export function INoteIStorableIDataResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): INoteIStorableIDataResponse {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        
        'isOk': !exists(json, 'isOk') ? undefined : json['isOk'],
        'message': !exists(json, 'message') ? undefined : json['message'],
        'data': !exists(json, 'data') ? undefined : INoteIStorableFromJSON(json['data']),
        'executionMilliseconds': !exists(json, 'executionMilliseconds') ? undefined : json['executionMilliseconds'],
        'totalMilliseconds': !exists(json, 'totalMilliseconds') ? undefined : json['totalMilliseconds'],
        'remoteCallMilliseconds': !exists(json, 'remoteCallMilliseconds') ? undefined : json['remoteCallMilliseconds'],
        'executor': !exists(json, 'executor') ? undefined : json['executor'],
    };
}

export function INoteIStorableIDataResponseToJSON(value?: INoteIStorableIDataResponse | null): any {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        
        'data': INoteIStorableToJSON(value.data),
        'executionMilliseconds': value.executionMilliseconds,
        'totalMilliseconds': value.totalMilliseconds,
        'remoteCallMilliseconds': value.remoteCallMilliseconds,
        'executor': value.executor,
    };
}


