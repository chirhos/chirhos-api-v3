/* tslint:disable */
/* eslint-disable */
/**
 * ChiRhos API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: v1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime';
/**
 * 
 * @export
 * @interface ApiEventAttendanceMultipleRequest
 */
export interface ApiEventAttendanceMultipleRequest {
    /**
     * 
     * @type {string}
     * @memberof ApiEventAttendanceMultipleRequest
     */
    eventEntityId?: string;
    /**
     * 
     * @type {Array<string>}
     * @memberof ApiEventAttendanceMultipleRequest
     */
    personEntityIds?: Array<string> | null;
    /**
     * 
     * @type {string}
     * @memberof ApiEventAttendanceMultipleRequest
     */
    personUniqueCode?: string | null;
}

export function ApiEventAttendanceMultipleRequestFromJSON(json: any): ApiEventAttendanceMultipleRequest {
    return ApiEventAttendanceMultipleRequestFromJSONTyped(json, false);
}

export function ApiEventAttendanceMultipleRequestFromJSONTyped(json: any, ignoreDiscriminator: boolean): ApiEventAttendanceMultipleRequest {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        
        'eventEntityId': !exists(json, 'eventEntityId') ? undefined : json['eventEntityId'],
        'personEntityIds': !exists(json, 'personEntityIds') ? undefined : json['personEntityIds'],
        'personUniqueCode': !exists(json, 'personUniqueCode') ? undefined : json['personUniqueCode'],
    };
}

export function ApiEventAttendanceMultipleRequestToJSON(value?: ApiEventAttendanceMultipleRequest | null): any {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        
        'eventEntityId': value.eventEntityId,
        'personEntityIds': value.personEntityIds,
        'personUniqueCode': value.personUniqueCode,
    };
}


