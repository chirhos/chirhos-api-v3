/* tslint:disable */
/* eslint-disable */
/**
 * ChiRhos API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: v1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime';
import {
    EventTypeDefinition,
    EventTypeDefinitionFromJSON,
    EventTypeDefinitionFromJSONTyped,
    EventTypeDefinitionToJSON,
    RoleDefinition,
    RoleDefinitionFromJSON,
    RoleDefinitionFromJSONTyped,
    RoleDefinitionToJSON,
    SecurityEntryDefinition,
    SecurityEntryDefinitionFromJSON,
    SecurityEntryDefinitionFromJSONTyped,
    SecurityEntryDefinitionToJSON,
} from './';

/**
 * 
 * @export
 * @interface EventTypeDefinitionRoleDefinitionSecurityEntryDefinitionGroupTypeDefinition
 */
export interface EventTypeDefinitionRoleDefinitionSecurityEntryDefinitionGroupTypeDefinition {
    /**
     * 
     * @type {string}
     * @memberof EventTypeDefinitionRoleDefinitionSecurityEntryDefinitionGroupTypeDefinition
     */
    name: string;
    /**
     * 
     * @type {string}
     * @memberof EventTypeDefinitionRoleDefinitionSecurityEntryDefinitionGroupTypeDefinition
     */
    externalId?: string | null;
    /**
     * 
     * @type {Array<EventTypeDefinitionRoleDefinitionSecurityEntryDefinitionGroupTypeDefinition>}
     * @memberof EventTypeDefinitionRoleDefinitionSecurityEntryDefinitionGroupTypeDefinition
     */
    children?: Array<EventTypeDefinitionRoleDefinitionSecurityEntryDefinitionGroupTypeDefinition> | null;
    /**
     * 
     * @type {Array<EventTypeDefinition>}
     * @memberof EventTypeDefinitionRoleDefinitionSecurityEntryDefinitionGroupTypeDefinition
     */
    eventTypes?: Array<EventTypeDefinition> | null;
    /**
     * 
     * @type {Array<RoleDefinition>}
     * @memberof EventTypeDefinitionRoleDefinitionSecurityEntryDefinitionGroupTypeDefinition
     */
    roles?: Array<RoleDefinition> | null;
    /**
     * 
     * @type {Array<SecurityEntryDefinition>}
     * @memberof EventTypeDefinitionRoleDefinitionSecurityEntryDefinitionGroupTypeDefinition
     */
    groupPermissions?: Array<SecurityEntryDefinition> | null;
}

export function EventTypeDefinitionRoleDefinitionSecurityEntryDefinitionGroupTypeDefinitionFromJSON(json: any): EventTypeDefinitionRoleDefinitionSecurityEntryDefinitionGroupTypeDefinition {
    return EventTypeDefinitionRoleDefinitionSecurityEntryDefinitionGroupTypeDefinitionFromJSONTyped(json, false);
}

export function EventTypeDefinitionRoleDefinitionSecurityEntryDefinitionGroupTypeDefinitionFromJSONTyped(json: any, ignoreDiscriminator: boolean): EventTypeDefinitionRoleDefinitionSecurityEntryDefinitionGroupTypeDefinition {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        
        'name': json['name'],
        'externalId': !exists(json, 'externalId') ? undefined : json['externalId'],
        'children': !exists(json, 'children') ? undefined : (json['children'] === null ? null : (json['children'] as Array<any>).map(EventTypeDefinitionRoleDefinitionSecurityEntryDefinitionGroupTypeDefinitionFromJSON)),
        'eventTypes': !exists(json, 'eventTypes') ? undefined : (json['eventTypes'] === null ? null : (json['eventTypes'] as Array<any>).map(EventTypeDefinitionFromJSON)),
        'roles': !exists(json, 'roles') ? undefined : (json['roles'] === null ? null : (json['roles'] as Array<any>).map(RoleDefinitionFromJSON)),
        'groupPermissions': !exists(json, 'groupPermissions') ? undefined : (json['groupPermissions'] === null ? null : (json['groupPermissions'] as Array<any>).map(SecurityEntryDefinitionFromJSON)),
    };
}

export function EventTypeDefinitionRoleDefinitionSecurityEntryDefinitionGroupTypeDefinitionToJSON(value?: EventTypeDefinitionRoleDefinitionSecurityEntryDefinitionGroupTypeDefinition | null): any {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        
        'name': value.name,
        'externalId': value.externalId,
        'children': value.children === undefined ? undefined : (value.children === null ? null : (value.children as Array<any>).map(EventTypeDefinitionRoleDefinitionSecurityEntryDefinitionGroupTypeDefinitionToJSON)),
        'eventTypes': value.eventTypes === undefined ? undefined : (value.eventTypes === null ? null : (value.eventTypes as Array<any>).map(EventTypeDefinitionToJSON)),
        'roles': value.roles === undefined ? undefined : (value.roles === null ? null : (value.roles as Array<any>).map(RoleDefinitionToJSON)),
        'groupPermissions': value.groupPermissions === undefined ? undefined : (value.groupPermissions === null ? null : (value.groupPermissions as Array<any>).map(SecurityEntryDefinitionToJSON)),
    };
}


