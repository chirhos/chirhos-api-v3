/* tslint:disable */
/* eslint-disable */
/**
 * ChiRhos API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: v1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime';
import {
    Integration,
    IntegrationFromJSON,
    IntegrationFromJSONTyped,
    IntegrationToJSON,
    PropertyPreset,
    PropertyPresetFromJSON,
    PropertyPresetFromJSONTyped,
    PropertyPresetToJSON,
} from './';

/**
 * 
 * @export
 * @interface EventTypeConstruct
 */
export interface EventTypeConstruct {
    /**
     * 
     * @type {string}
     * @memberof EventTypeConstruct
     */
    belongsTo?: string;
    /**
     * 
     * @type {string}
     * @memberof EventTypeConstruct
     */
    name: string;
    /**
     * 
     * @type {Array<string>}
     * @memberof EventTypeConstruct
     */
    headCounts?: Array<string> | null;
    /**
     * 
     * @type {{ [key: string]: PropertyPreset; }}
     * @memberof EventTypeConstruct
     */
    notePropertyPresets?: { [key: string]: PropertyPreset; } | null;
    /**
     * 
     * @type {string}
     * @memberof EventTypeConstruct
     */
    entityId?: string;
    /**
     * 
     * @type {{ [key: string]: object; }}
     * @memberof EventTypeConstruct
     */
    properties?: { [key: string]: object; } | null;
    /**
     * 
     * @type {Integration}
     * @memberof EventTypeConstruct
     */
    integration?: Integration;
}

export function EventTypeConstructFromJSON(json: any): EventTypeConstruct {
    return EventTypeConstructFromJSONTyped(json, false);
}

export function EventTypeConstructFromJSONTyped(json: any, ignoreDiscriminator: boolean): EventTypeConstruct {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        
        'belongsTo': !exists(json, 'belongsTo') ? undefined : json['belongsTo'],
        'name': json['name'],
        'headCounts': !exists(json, 'headCounts') ? undefined : json['headCounts'],
        'notePropertyPresets': !exists(json, 'notePropertyPresets') ? undefined : (json['notePropertyPresets'] === null ? null : mapValues(json['notePropertyPresets'], PropertyPresetFromJSON)),
        'entityId': !exists(json, 'entityId') ? undefined : json['entityId'],
        'properties': !exists(json, 'properties') ? undefined : json['properties'],
        'integration': !exists(json, 'integration') ? undefined : IntegrationFromJSON(json['integration']),
    };
}

export function EventTypeConstructToJSON(value?: EventTypeConstruct | null): any {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        
        'belongsTo': value.belongsTo,
        'name': value.name,
        'headCounts': value.headCounts,
        'notePropertyPresets': value.notePropertyPresets === undefined ? undefined : (value.notePropertyPresets === null ? null : mapValues(value.notePropertyPresets, PropertyPresetToJSON)),
        'entityId': value.entityId,
        'properties': value.properties,
        'integration': IntegrationToJSON(value.integration),
    };
}


