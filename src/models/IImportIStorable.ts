/* tslint:disable */
/* eslint-disable */
/**
 * ChiRhos API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: v1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime';
import {
    IImport,
    IImportFromJSON,
    IImportFromJSONTyped,
    IImportToJSON,
} from './';

/**
 * 
 * @export
 * @interface IImportIStorable
 */
export interface IImportIStorable {
    /**
     * 
     * @type {IImport}
     * @memberof IImportIStorable
     */
    entity?: IImport;
    /**
     * 
     * @type {string}
     * @memberof IImportIStorable
     */
    readonly tenant: string;
    /**
     * 
     * @type {string}
     * @memberof IImportIStorable
     */
    readonly entityType: string;
    /**
     * 
     * @type {Date}
     * @memberof IImportIStorable
     */
    from?: Date;
    /**
     * 
     * @type {Date}
     * @memberof IImportIStorable
     */
    to?: Date | null;
    /**
     * 
     * @type {string}
     * @memberof IImportIStorable
     */
    modifiedByPerson?: string;
    /**
     * 
     * @type {boolean}
     * @memberof IImportIStorable
     */
    isDeleted?: boolean;
}

export function IImportIStorableFromJSON(json: any): IImportIStorable {
    return IImportIStorableFromJSONTyped(json, false);
}

export function IImportIStorableFromJSONTyped(json: any, ignoreDiscriminator: boolean): IImportIStorable {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        
        'entity': !exists(json, 'entity') ? undefined : IImportFromJSON(json['entity']),
        'tenant': json['tenant'],
        'entityType': json['entityType'],
        'from': !exists(json, 'from') ? undefined : (new Date(json['from'])),
        'to': !exists(json, 'to') ? undefined : (json['to'] === null ? null : new Date(json['to'])),
        'modifiedByPerson': !exists(json, 'modifiedByPerson') ? undefined : json['modifiedByPerson'],
        'isDeleted': !exists(json, 'isDeleted') ? undefined : json['isDeleted'],
    };
}

export function IImportIStorableToJSON(value?: IImportIStorable | null): any {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        
        'entity': IImportToJSON(value.entity),
        'from': value.from === undefined ? undefined : (value.from.toISOString()),
        'to': value.to === undefined ? undefined : (value.to === null ? null : value.to.toISOString()),
        'modifiedByPerson': value.modifiedByPerson,
        'isDeleted': value.isDeleted,
    };
}


