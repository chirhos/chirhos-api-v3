/* tslint:disable */
/* eslint-disable */
/**
 * ChiRhos API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: v1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime';
import {
    ISubscriptionIStorable,
    ISubscriptionIStorableFromJSON,
    ISubscriptionIStorableFromJSONTyped,
    ISubscriptionIStorableToJSON,
} from './';

/**
 * 
 * @export
 * @interface ISubscriptionIStorableIEnumerableIDataResponse
 */
export interface ISubscriptionIStorableIEnumerableIDataResponse {
    /**
     * 
     * @type {boolean}
     * @memberof ISubscriptionIStorableIEnumerableIDataResponse
     */
    readonly isOk?: boolean;
    /**
     * 
     * @type {string}
     * @memberof ISubscriptionIStorableIEnumerableIDataResponse
     */
    readonly message?: string | null;
    /**
     * 
     * @type {Array<ISubscriptionIStorable>}
     * @memberof ISubscriptionIStorableIEnumerableIDataResponse
     */
    readonly data?: Array<ISubscriptionIStorable> | null;
    /**
     * 
     * @type {number}
     * @memberof ISubscriptionIStorableIEnumerableIDataResponse
     */
    executionMilliseconds?: number;
    /**
     * 
     * @type {number}
     * @memberof ISubscriptionIStorableIEnumerableIDataResponse
     */
    totalMilliseconds?: number;
    /**
     * 
     * @type {number}
     * @memberof ISubscriptionIStorableIEnumerableIDataResponse
     */
    remoteCallMilliseconds?: number;
    /**
     * 
     * @type {string}
     * @memberof ISubscriptionIStorableIEnumerableIDataResponse
     */
    executor?: string;
}

export function ISubscriptionIStorableIEnumerableIDataResponseFromJSON(json: any): ISubscriptionIStorableIEnumerableIDataResponse {
    return ISubscriptionIStorableIEnumerableIDataResponseFromJSONTyped(json, false);
}

export function ISubscriptionIStorableIEnumerableIDataResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): ISubscriptionIStorableIEnumerableIDataResponse {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        
        'isOk': !exists(json, 'isOk') ? undefined : json['isOk'],
        'message': !exists(json, 'message') ? undefined : json['message'],
        'data': !exists(json, 'data') ? undefined : (json['data'] === null ? null : (json['data'] as Array<any>).map(ISubscriptionIStorableFromJSON)),
        'executionMilliseconds': !exists(json, 'executionMilliseconds') ? undefined : json['executionMilliseconds'],
        'totalMilliseconds': !exists(json, 'totalMilliseconds') ? undefined : json['totalMilliseconds'],
        'remoteCallMilliseconds': !exists(json, 'remoteCallMilliseconds') ? undefined : json['remoteCallMilliseconds'],
        'executor': !exists(json, 'executor') ? undefined : json['executor'],
    };
}

export function ISubscriptionIStorableIEnumerableIDataResponseToJSON(value?: ISubscriptionIStorableIEnumerableIDataResponse | null): any {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        
        'executionMilliseconds': value.executionMilliseconds,
        'totalMilliseconds': value.totalMilliseconds,
        'remoteCallMilliseconds': value.remoteCallMilliseconds,
        'executor': value.executor,
    };
}


