/* tslint:disable */
/* eslint-disable */
/**
 * ChiRhos API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: v1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime';
import {
    IGroupIStorable,
    IGroupIStorableFromJSON,
    IGroupIStorableFromJSONTyped,
    IGroupIStorableToJSON,
} from './';

/**
 * 
 * @export
 * @interface IGroupIStorablePagedResponse
 */
export interface IGroupIStorablePagedResponse {
    /**
     * 
     * @type {number}
     * @memberof IGroupIStorablePagedResponse
     */
    totalCount?: number;
    /**
     * 
     * @type {number}
     * @memberof IGroupIStorablePagedResponse
     */
    page?: number;
    /**
     * 
     * @type {number}
     * @memberof IGroupIStorablePagedResponse
     */
    pageSize?: number;
    /**
     * 
     * @type {string}
     * @memberof IGroupIStorablePagedResponse
     */
    executor?: string;
    /**
     * 
     * @type {boolean}
     * @memberof IGroupIStorablePagedResponse
     */
    readonly isOk?: boolean;
    /**
     * 
     * @type {string}
     * @memberof IGroupIStorablePagedResponse
     */
    message?: string | null;
    /**
     * 
     * @type {Array<IGroupIStorable>}
     * @memberof IGroupIStorablePagedResponse
     */
    data?: Array<IGroupIStorable> | null;
    /**
     * 
     * @type {number}
     * @memberof IGroupIStorablePagedResponse
     */
    executionMilliseconds?: number;
    /**
     * 
     * @type {number}
     * @memberof IGroupIStorablePagedResponse
     */
    remoteCallMilliseconds?: number;
    /**
     * 
     * @type {number}
     * @memberof IGroupIStorablePagedResponse
     */
    totalMilliseconds?: number;
}

export function IGroupIStorablePagedResponseFromJSON(json: any): IGroupIStorablePagedResponse {
    return IGroupIStorablePagedResponseFromJSONTyped(json, false);
}

export function IGroupIStorablePagedResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): IGroupIStorablePagedResponse {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        
        'totalCount': !exists(json, 'totalCount') ? undefined : json['totalCount'],
        'page': !exists(json, 'page') ? undefined : json['page'],
        'pageSize': !exists(json, 'pageSize') ? undefined : json['pageSize'],
        'executor': !exists(json, 'executor') ? undefined : json['executor'],
        'isOk': !exists(json, 'isOk') ? undefined : json['isOk'],
        'message': !exists(json, 'message') ? undefined : json['message'],
        'data': !exists(json, 'data') ? undefined : (json['data'] === null ? null : (json['data'] as Array<any>).map(IGroupIStorableFromJSON)),
        'executionMilliseconds': !exists(json, 'executionMilliseconds') ? undefined : json['executionMilliseconds'],
        'remoteCallMilliseconds': !exists(json, 'remoteCallMilliseconds') ? undefined : json['remoteCallMilliseconds'],
        'totalMilliseconds': !exists(json, 'totalMilliseconds') ? undefined : json['totalMilliseconds'],
    };
}

export function IGroupIStorablePagedResponseToJSON(value?: IGroupIStorablePagedResponse | null): any {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        
        'totalCount': value.totalCount,
        'page': value.page,
        'pageSize': value.pageSize,
        'executor': value.executor,
        'message': value.message,
        'data': value.data === undefined ? undefined : (value.data === null ? null : (value.data as Array<any>).map(IGroupIStorableToJSON)),
        'executionMilliseconds': value.executionMilliseconds,
        'remoteCallMilliseconds': value.remoteCallMilliseconds,
        'totalMilliseconds': value.totalMilliseconds,
    };
}


