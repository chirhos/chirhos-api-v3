/* tslint:disable */
/* eslint-disable */
/**
 * ChiRhos API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: v1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime';
import {
    EventTypeDefinitionRoleDefinitionSecurityEntryDefinitionGroupTypeDefinition,
    EventTypeDefinitionRoleDefinitionSecurityEntryDefinitionGroupTypeDefinitionFromJSON,
    EventTypeDefinitionRoleDefinitionSecurityEntryDefinitionGroupTypeDefinitionFromJSONTyped,
    EventTypeDefinitionRoleDefinitionSecurityEntryDefinitionGroupTypeDefinitionToJSON,
    Integration,
    IntegrationFromJSON,
    IntegrationFromJSONTyped,
    IntegrationToJSON,
} from './';

/**
 * 
 * @export
 * @interface ITemplate
 */
export interface ITemplate {
    /**
     * 
     * @type {string}
     * @memberof ITemplate
     */
    readonly name: string;
    /**
     * 
     * @type {EventTypeDefinitionRoleDefinitionSecurityEntryDefinitionGroupTypeDefinition}
     * @memberof ITemplate
     */
    root?: EventTypeDefinitionRoleDefinitionSecurityEntryDefinitionGroupTypeDefinition;
    /**
     * 
     * @type {string}
     * @memberof ITemplate
     */
    readonly subscriptionEntityId: string;
    /**
     * 
     * @type {string}
     * @memberof ITemplate
     */
    readonly entityId?: string;
    /**
     * 
     * @type {{ [key: string]: object; }}
     * @memberof ITemplate
     */
    properties?: { [key: string]: object; } | null;
    /**
     * 
     * @type {Integration}
     * @memberof ITemplate
     */
    integration?: Integration;
}

export function ITemplateFromJSON(json: any): ITemplate {
    return ITemplateFromJSONTyped(json, false);
}

export function ITemplateFromJSONTyped(json: any, ignoreDiscriminator: boolean): ITemplate {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        
        'name': json['name'],
        'root': !exists(json, 'root') ? undefined : EventTypeDefinitionRoleDefinitionSecurityEntryDefinitionGroupTypeDefinitionFromJSON(json['root']),
        'subscriptionEntityId': json['subscriptionEntityId'],
        'entityId': !exists(json, 'entityId') ? undefined : json['entityId'],
        'properties': !exists(json, 'properties') ? undefined : json['properties'],
        'integration': !exists(json, 'integration') ? undefined : IntegrationFromJSON(json['integration']),
    };
}

export function ITemplateToJSON(value?: ITemplate | null): any {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        
        'root': EventTypeDefinitionRoleDefinitionSecurityEntryDefinitionGroupTypeDefinitionToJSON(value.root),
        'properties': value.properties,
        'integration': IntegrationToJSON(value.integration),
    };
}


