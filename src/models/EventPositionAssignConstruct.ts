/* tslint:disable */
/* eslint-disable */
/**
 * ChiRhos API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: v1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime';
import {
    Integration,
    IntegrationFromJSON,
    IntegrationFromJSONTyped,
    IntegrationToJSON,
} from './';

/**
 * 
 * @export
 * @interface EventPositionAssignConstruct
 */
export interface EventPositionAssignConstruct {
    /**
     * 
     * @type {string}
     * @memberof EventPositionAssignConstruct
     */
    readonly belongsToGroup?: string;
    /**
     * 
     * @type {string}
     * @memberof EventPositionAssignConstruct
     */
    readonly belongsToEvent?: string;
    /**
     * 
     * @type {string}
     * @memberof EventPositionAssignConstruct
     */
    readonly fromPersonEntityId?: string;
    /**
     * 
     * @type {string}
     * @memberof EventPositionAssignConstruct
     */
    toPersonEntityId?: string;
    /**
     * 
     * @type {Date}
     * @memberof EventPositionAssignConstruct
     */
    readonly accepted?: Date | null;
    /**
     * 
     * @type {string}
     * @memberof EventPositionAssignConstruct
     */
    belongsTo?: string;
    /**
     * 
     * @type {string}
     * @memberof EventPositionAssignConstruct
     */
    entityId?: string;
    /**
     * 
     * @type {{ [key: string]: object; }}
     * @memberof EventPositionAssignConstruct
     */
    properties?: { [key: string]: object; } | null;
    /**
     * 
     * @type {Integration}
     * @memberof EventPositionAssignConstruct
     */
    integration?: Integration;
}

export function EventPositionAssignConstructFromJSON(json: any): EventPositionAssignConstruct {
    return EventPositionAssignConstructFromJSONTyped(json, false);
}

export function EventPositionAssignConstructFromJSONTyped(json: any, ignoreDiscriminator: boolean): EventPositionAssignConstruct {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        
        'belongsToGroup': !exists(json, 'belongsToGroup') ? undefined : json['belongsToGroup'],
        'belongsToEvent': !exists(json, 'belongsToEvent') ? undefined : json['belongsToEvent'],
        'fromPersonEntityId': !exists(json, 'fromPersonEntityId') ? undefined : json['fromPersonEntityId'],
        'toPersonEntityId': !exists(json, 'toPersonEntityId') ? undefined : json['toPersonEntityId'],
        'accepted': !exists(json, 'accepted') ? undefined : (json['accepted'] === null ? null : new Date(json['accepted'])),
        'belongsTo': !exists(json, 'belongsTo') ? undefined : json['belongsTo'],
        'entityId': !exists(json, 'entityId') ? undefined : json['entityId'],
        'properties': !exists(json, 'properties') ? undefined : json['properties'],
        'integration': !exists(json, 'integration') ? undefined : IntegrationFromJSON(json['integration']),
    };
}

export function EventPositionAssignConstructToJSON(value?: EventPositionAssignConstruct | null): any {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        
        'toPersonEntityId': value.toPersonEntityId,
        'belongsTo': value.belongsTo,
        'entityId': value.entityId,
        'properties': value.properties,
        'integration': IntegrationToJSON(value.integration),
    };
}


