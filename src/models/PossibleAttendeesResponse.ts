/* tslint:disable */
/* eslint-disable */
/**
 * ChiRhos API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: v1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime';
import {
    Attendee,
    AttendeeFromJSON,
    AttendeeFromJSONTyped,
    AttendeeToJSON,
    IEventAttendance,
    IEventAttendanceFromJSON,
    IEventAttendanceFromJSONTyped,
    IEventAttendanceToJSON,
    PersonSummary,
    PersonSummaryFromJSON,
    PersonSummaryFromJSONTyped,
    PersonSummaryToJSON,
} from './';

/**
 * 
 * @export
 * @interface PossibleAttendeesResponse
 */
export interface PossibleAttendeesResponse {
    /**
     * 
     * @type {boolean}
     * @memberof PossibleAttendeesResponse
     */
    selfCanAttend?: boolean;
    /**
     * 
     * @type {IEventAttendance}
     * @memberof PossibleAttendeesResponse
     */
    selfAttendance?: IEventAttendance;
    /**
     * 
     * @type {Array<Attendee>}
     * @memberof PossibleAttendeesResponse
     */
    checkedIn?: Array<Attendee> | null;
    /**
     * 
     * @type {Array<Attendee>}
     * @memberof PossibleAttendeesResponse
     */
    checkedOut?: Array<Attendee> | null;
    /**
     * 
     * @type {Array<Attendee>}
     * @memberof PossibleAttendeesResponse
     */
    attending?: Array<Attendee> | null;
    /**
     * 
     * @type {Array<PersonSummary>}
     * @memberof PossibleAttendeesResponse
     */
    notAttending?: Array<PersonSummary> | null;
    /**
     * 
     * @type {Array<PersonSummary>}
     * @memberof PossibleAttendeesResponse
     */
    maybeAttending?: Array<PersonSummary> | null;
    /**
     * 
     * @type {Array<PersonSummary>}
     * @memberof PossibleAttendeesResponse
     */
    awaitingRsvp?: Array<PersonSummary> | null;
}

export function PossibleAttendeesResponseFromJSON(json: any): PossibleAttendeesResponse {
    return PossibleAttendeesResponseFromJSONTyped(json, false);
}

export function PossibleAttendeesResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): PossibleAttendeesResponse {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        
        'selfCanAttend': !exists(json, 'selfCanAttend') ? undefined : json['selfCanAttend'],
        'selfAttendance': !exists(json, 'selfAttendance') ? undefined : IEventAttendanceFromJSON(json['selfAttendance']),
        'checkedIn': !exists(json, 'checkedIn') ? undefined : (json['checkedIn'] === null ? null : (json['checkedIn'] as Array<any>).map(AttendeeFromJSON)),
        'checkedOut': !exists(json, 'checkedOut') ? undefined : (json['checkedOut'] === null ? null : (json['checkedOut'] as Array<any>).map(AttendeeFromJSON)),
        'attending': !exists(json, 'attending') ? undefined : (json['attending'] === null ? null : (json['attending'] as Array<any>).map(AttendeeFromJSON)),
        'notAttending': !exists(json, 'notAttending') ? undefined : (json['notAttending'] === null ? null : (json['notAttending'] as Array<any>).map(PersonSummaryFromJSON)),
        'maybeAttending': !exists(json, 'maybeAttending') ? undefined : (json['maybeAttending'] === null ? null : (json['maybeAttending'] as Array<any>).map(PersonSummaryFromJSON)),
        'awaitingRsvp': !exists(json, 'awaitingRsvp') ? undefined : (json['awaitingRsvp'] === null ? null : (json['awaitingRsvp'] as Array<any>).map(PersonSummaryFromJSON)),
    };
}

export function PossibleAttendeesResponseToJSON(value?: PossibleAttendeesResponse | null): any {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        
        'selfCanAttend': value.selfCanAttend,
        'selfAttendance': IEventAttendanceToJSON(value.selfAttendance),
        'checkedIn': value.checkedIn === undefined ? undefined : (value.checkedIn === null ? null : (value.checkedIn as Array<any>).map(AttendeeToJSON)),
        'checkedOut': value.checkedOut === undefined ? undefined : (value.checkedOut === null ? null : (value.checkedOut as Array<any>).map(AttendeeToJSON)),
        'attending': value.attending === undefined ? undefined : (value.attending === null ? null : (value.attending as Array<any>).map(AttendeeToJSON)),
        'notAttending': value.notAttending === undefined ? undefined : (value.notAttending === null ? null : (value.notAttending as Array<any>).map(PersonSummaryToJSON)),
        'maybeAttending': value.maybeAttending === undefined ? undefined : (value.maybeAttending === null ? null : (value.maybeAttending as Array<any>).map(PersonSummaryToJSON)),
        'awaitingRsvp': value.awaitingRsvp === undefined ? undefined : (value.awaitingRsvp === null ? null : (value.awaitingRsvp as Array<any>).map(PersonSummaryToJSON)),
    };
}


