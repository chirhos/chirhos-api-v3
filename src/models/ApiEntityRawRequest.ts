/* tslint:disable */
/* eslint-disable */
/**
 * ChiRhos API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: v1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime';
/**
 * 
 * @export
 * @interface ApiEntityRawRequest
 */
export interface ApiEntityRawRequest {
    /**
     * 
     * @type {string}
     * @memberof ApiEntityRawRequest
     */
    entityId?: string;
}

export function ApiEntityRawRequestFromJSON(json: any): ApiEntityRawRequest {
    return ApiEntityRawRequestFromJSONTyped(json, false);
}

export function ApiEntityRawRequestFromJSONTyped(json: any, ignoreDiscriminator: boolean): ApiEntityRawRequest {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        
        'entityId': !exists(json, 'entityId') ? undefined : json['entityId'],
    };
}

export function ApiEntityRawRequestToJSON(value?: ApiEntityRawRequest | null): any {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        
        'entityId': value.entityId,
    };
}


