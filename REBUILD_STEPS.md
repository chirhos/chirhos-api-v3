1. generate by a POST to http://api.openapi-generator.tech/api/gen/clients/typescript-fetch with Content-Type of application/json and body:

{
    "options":
    {
      "sortParamsByRequiredFlag": true,
      "ensureUniqueParams": true,
      "allowUnicodeIdentifiers": false,
      "modelPropertyNaming": "camelCase",
      "supportsES6": true,
      "npmName" : "@chirhos/chirhos-api-v3",
  		"npmVersion": "0.1.3",
      "npmRepository": "git+https://bitbucket.org/chirhos/chirhos-api-v3/",
      "snapshot": false,
      "withInterfaces": true
    },
    "openAPIUrl": "https://chirhos-sandbox.azurewebsites.net/swagger/v1/swagger.json"
  }

2. replace the src folder in this repo with the src folder from the generated
-- 3. search for 'tenantpinghead' and delete any of those methods (HEAD is not supported by Fetch API correctly)
4. update the version in package.json
5. make sure registry is correct in npm
6. run: npm run build
